$(document).ready(function (){
    // Changes the icons in the footer to grayscale on click and opens their link in another window
    $(".icon-group").on("click", "a[href^='https://']", function () {
        $(this).css({
            "filter": "grayscale(100%)",
            "-webkit-filter": "grayscale(100%)"
        });
        $(this).attr("target", "_blank");
    });

    // The following statements are simple click methods on buttons to visit another page
    $("#login-btn").click(function() {
        window.location="html/logged-in-posts.html";
    });

    $("#sign-up").click(function() {
        window.location="html/logged-in-posts.html";
    });

    $("#return-btn").click(function() {
        window.location="../list.html";
    });

    // This is the first complete user interaction including the statement directly below and the one following it
    // Hides the comment button and shows the textarea for the user to create a comment
    $("#comment-btn").click(function() {
        $("#comment-btn").hide();
        $(".wrapper-user-comment").show();
    });

    // Submits the comment if it contains text
    $("#submit-comment").click(function(e) {
        e.preventDefault();
        var text1 = $("#user-comment").val();
        text1 = text1.trim();
        if (text1.length > 0) {
            $(".wrapper-user-comment").after('<hr class="comment-hr">', '<div class="individual-comment" ' +
                'id="test-comment"><p class="comment-element" id="test-add"><strong>test_user</strong> 1 second ago' +
                '</p></div>');
            var userTitle = $("#test-comment").children().first();
            userTitle.after('<p class="comment-element" id="test-input"></p>');
            $("#test-input").append(text1);
            var userInput = userTitle.siblings().first();
            userInput.after('<p class="comment-element"><a href="#reply">Reply</a></p>');
            $("#user-comment").val('');
            $("#comment-btn").show();
            $(".wrapper-user-comment").hide();
        }
        else {
            window.alert("You cannot submit an empty comment")
        }
    });

    // This statement submits a search from the homepage after one of the countries in the country list on the homepage
    // is clicked
    $(".country-list").on("click", "a", function() {
       var countryName = $(this).text();
       $("#search").val(function() {
           return this.value + countryName;
       });
       $("#search-btn").click();
    });

    $(".country").hover( function() {
            $(this).css({"color": "blue"});
        },
        function() {
            $(this).css({"color": ""});
    });

    // This method is the second complete user interaction
    // Once the english button in the footer is clicked it changes color and a german button is created. If that german
    // button is clicked, then the homepage is changed to the german version
    $(".footer-bar").on("click", "button", function() {
        if ($(this).text() != "German" && $(this).parent().siblings().last().children().first().text() != "German") {
            $(this).css({"color": "white", "background-color": "blue"});
            $(this).parent().after("<li class='footer-element'><button type='button'>German</button></li>");
        }
        if ($(this).text() == "German") {
            window.location="../index-german.html";
            // $(this).parent().prev().css({"display": "none"});
        }
    });
    checkQueryString();
});

// This function checks whether a correct search term has been entered
function checkQueryString(){
    var queryString = window.location.search;
    var urlParams = new URLSearchParams(queryString);
    if(urlParams.has("search")) {
        var keyword = urlParams.get("search");
        if (!(keyword == 'german' || keyword == 'German' || keyword == 'Germany' || keyword == 'Deutschland')) {
            $(".search-posts").hide();
            if (keyword.length > 0) {
                window.alert("It looks like we're missing content related to " + keyword + ". Sorry about that. If " +
                    "you know any words related to this topic, we'd love it if you could add them. Thanks!");
            }
            else {
                window.alert("Please enter a search term");
            }
        }
        else {
            $(".search-posts").show();
            $(".post-list-header h3").append(" for '" + keyword + "'");
        }
    }
}